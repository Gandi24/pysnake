from django.contrib import admin
from teams.models import Team

class TeamAdmin(admin.ModelAdmin):
    fields = ('snake_name',)
    list_display = ('snake_name', 'secret_key', 'best_randomkonda_result', 'best_blind_mamba_result', 'best_cobra_star_result')

admin.site.register(Team, TeamAdmin)