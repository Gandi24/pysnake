# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('teams', '0003_auto_20141007_2214'),
    ]

    operations = [
        migrations.RenameField(
            model_name='team',
            old_name='best_result',
            new_name='best_blind_mamba_result',
        ),
        migrations.AddField(
            model_name='team',
            name='best_cobra_star_result',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='team',
            name='best_randomkonda_result',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
    ]
