# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('teams', '0002_auto_20141007_2208'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='best_result',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
