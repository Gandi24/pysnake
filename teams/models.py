from django.db import models
import random
import string

class Team(models.Model):
    snake_name = models.CharField(max_length=64)
    secret_key = models.CharField(max_length=8)
    best_snake = models.TextField(blank=True, null=True)
    best_randomkonda_result = models.PositiveIntegerField(default=0)
    best_blind_mamba_result = models.PositiveIntegerField(default=0)
    best_cobra_star_result = models.PositiveIntegerField(default=0)

    @property
    def best_results(self):
        return{
            'randomkonda': self.best_randomkonda_result,
            'blind_mamba': self.best_blind_mamba_result,
            'cobra_star': self.best_cobra_star_result,
        }

    def save(self, *args, **kwargs):
    	if not self.secret_key:
	    	self.secret_key = ''.join([random.choice(string.digits + string.letters) for i in range(8)])
    	return super(Team, self).save(*args, **kwargs)

  	def __unicode__(self):
  		return self.name + " <" + self.secret_key + ">"