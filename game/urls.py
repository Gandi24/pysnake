from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
from .views import HomeView, SubmitCodeView, GetBoardView, GetResultTable, AdminBoardView, SubmitTournamentView

urlpatterns = patterns('',
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^submit-code/$', SubmitCodeView.as_view(), name='submit_code'),
    url(r'^submit-tournament/$', SubmitTournamentView.as_view(), name='submit_tournament'),
    url(r'^get-board/$', GetBoardView.as_view(), name='get_board'),
    url(r'^get-results/$', GetResultTable.as_view(), name='get_results'),
    url(r'^admin-board/$', login_required(AdminBoardView.as_view()), name='admin_board'),
)
