from django.views.generic import View, FormView, TemplateView
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect
from .forms import SubmitCodeForm
from .tasks import calculate_game, calculate_tournament_game
from .models import GameHistory
from teams.models import Team
from utils.json_response import JsonResponse
import json
import uuid
from django.template import Context, loader

class HomeView(FormView):
	template_name = 'game/home.html'
	form_class = SubmitCodeForm

	def dispatch(self, request, *args, **kwargs):
		self.teams_list = Team.objects.all().order_by('-best_cobra_star_result').values('snake_name', 'best_randomkonda_result', 'best_blind_mamba_result', 'best_cobra_star_result')
		return super(HomeView, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, *args, **kwargs):
		context = super(HomeView, self).get_context_data(*args, **kwargs)
		context['teams_list'] = self.teams_list
		return context


class SubmitCodeView(FormView):
	form_class = SubmitCodeForm
	SNAKES = ['randomkonda', 'blind_mamba', 'cobra_star']

	def post(self, request, *args, **kwargs):
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		if form.is_valid():
			game_uuids = []
			for snake in self.SNAKES:
				snake_uuid = str(uuid.uuid1())
				game_uuids.append({'name': snake, 'uuid': snake_uuid})
				calculate_game.delay(request.POST.get('snake_code'), uuid=snake_uuid, enemy_snake_name=snake, secret_key=request.POST.get('secret_key'))
	 		return JsonResponse({'status': 'OK', 'game_uuids': game_uuids, 'csrf': request.POST.get('csrfmiddlewaretoken')})
		else:
			return JsonResponse({'status': 'ERROR', 'errors': form.errors})


class GetBoardView(View):
	def post(self, request, *args, **kwargs):
		try:
			game_history = GameHistory.objects.get(uuid=request.POST.get('uuid'))
		except GameHistory.DoesNotExist:
			return JsonResponse({
				'status': 'ERROR',
				'errors': 'Game history does not exist'
				})
		return JsonResponse({
			'status': 'OK',
			'moves_history': game_history.moves_history,
			'snake1_result': game_history.snake1_result,
			'snake2_result': game_history.snake2_result,
			'error': game_history.error
			})


class GetResultTable(View):
	def post(self, request, *args, **kwargs):
		t = loader.get_template('game/results_table.html')
		teams_list = Team.objects.all().order_by('-best_cobra_star_result').values('snake_name', 'best_randomkonda_result', 'best_blind_mamba_result', 'best_cobra_star_result')
		return JsonResponse({
			'status': 'OK',
			'results_table': t.render(Context({'teams_list': teams_list}))
			})

class AdminBoardView(TemplateView):
	template_name = 'game/admin_board.html'

	def dispatch(self, request, *args, **kwargs):
		self.teams_list = Team.objects.filter(best_snake__isnull=False).order_by('-best_cobra_star_result').values('id', 'snake_name', 'best_randomkonda_result', 'best_blind_mamba_result', 'best_cobra_star_result')
		return super(AdminBoardView, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, *args, **kwargs):
		context = super(AdminBoardView, self).get_context_data(*args, **kwargs)
		context['teams_list'] = self.teams_list
		return context


class SubmitTournamentView(TemplateView):

	def post(self, request, *args, **kwargs):
		game_uuids = []
		snake_uuid = str(uuid.uuid1())

		snakes_ids = request.POST.getlist('snakes[]')
		if len(snakes_ids) != 2:
			return JsonResponse({
				'status': 'ERROR',
				'errors': 'Choose exactly two snakes'
				})

		game_uuids.append({'name': 'tournament', 'uuid': snake_uuid})
		snake1_code = Team.objects.get(id=snakes_ids[0]).best_snake
		snake2_code = Team.objects.get(id=snakes_ids[1]).best_snake
		calculate_tournament_game.delay(snake1_code, snake2_code, uuid=snake_uuid)
 		return JsonResponse({'status': 'OK', 'game_uuids': game_uuids, 'csrf': request.POST.get('csrfmiddlewaretoken')})
