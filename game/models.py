from django.db import models
from teams.models import Team

class GameHistory(models.Model):
    team = models.ForeignKey(Team, null=True, blank=True)
    uuid = models.CharField(max_length=36)
    moves_history = models.TextField(blank=True, null=True)
    snake1_result = models.PositiveIntegerField(default=0)
    snake2_result = models.PositiveIntegerField(default=0)
    error = models.CharField(max_length=512, blank=True, null=True)