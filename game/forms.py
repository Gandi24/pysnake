from django import forms
from teams.models import Team
from django.core.exceptions import ValidationError

class SubmitCodeForm(forms.Form):
	secret_key = forms.CharField(label='Secret Key', max_length=8, required=True)
	snake_code = forms.CharField(label='Snake Code', max_length=10000, required=True)

	def clean_secret_key(self):
		secret_key = self.cleaned_data.get('secret_key')
		if not Team.objects.filter(secret_key=secret_key):
			raise ValidationError('%s is not a valid secret key' %secret_key)
		return secret_key
