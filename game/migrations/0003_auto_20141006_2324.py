# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('teams', '__first__'),
        ('game', '0002_auto_20141005_2234'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='gamehistory',
            name='snake_name',
        ),
        migrations.AddField(
            model_name='gamehistory',
            name='team',
            field=models.ForeignKey(blank=True, to='teams.Team', null=True),
            preserve_default=True,
        ),
    ]
