# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='GameHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('snake_name', models.CharField(max_length=64)),
                ('uuid', models.CharField(max_length=36)),
                ('moves_history', models.CharField(max_length=8192)),
                ('snake1_result', models.PositiveIntegerField()),
                ('snake2_result', models.PositiveIntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
