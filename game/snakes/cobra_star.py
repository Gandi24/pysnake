def move(snake1=None, snake2=None, food=None, data=None, board_width=None, board_height=None):
	from math import sqrt, pow
	from collections import namedtuple

	Point = namedtuple('Point', 'x y')

	def heuristic_cost_estimate(start, goal):
		return sqrt(pow(abs(start.x-goal.x), 2) + pow(abs(start.y-goal.y), 2))

	def get_direct_neighbours(point):
		return [
			Point(x=point.x+1, y=point.y),
			Point(x=point.x-1, y=point.y),
			Point(x=point.x, y=point.y+1),
			Point(x=point.x, y=point.y-1)
			]

	def neighbour_nodes(point):
		neighbours = []
		for new_point in get_direct_neighbours(point):
			if new_point in snake1.body or new_point in snake2.body or 0 > new_point.x >= board_width-1 or 0 > new_point.y >= board_height-1 or new_point in get_direct_neighbours(snake2.head):
				continue
			neighbours.append(new_point)
		return neighbours

	def reconstruct_path(came_from, current_node):
		if current_node in came_from:
			p = reconstruct_path(came_from, came_from[current_node])
			p.append(current_node)
			return p
		else:
			return [current_node]

	def get_direction(point1, point2):
		if point2.x == point1.x+1 and point2.y == point1.y:
			return 'r'
		elif point2.x == point1.x-1 and point2.y == point1.y:
			return 'l'
		elif point2.x == point1.x and point2.y == point1.y+1:
			return 'd'
		elif point2.x == point1.x and point2.y == point1.y-1:
			return 'u'
		return 'u'

	start = snake1.head
	goal = food
	closedset = []
	openset = [start]
	came_from = {}

	g_score = {start: 0}
	f_score = {start: g_score[start] + heuristic_cost_estimate(start, goal)}

	possibility_counter = 0
	while openset:
		possibility_counter += 1
		if possibility_counter > 500:
			break
		current = min(openset, key=lambda x: f_score.get(x))
		if current == goal:
			path = reconstruct_path(came_from, goal)
			next_point = path[1]
			direction = get_direction(start, next_point)
			return (direction, data)

		openset.remove(current)
		closedset.append(current)

		for neighbour in neighbour_nodes(current):
			if neighbour in closedset:
				continue
			tentative_g_score = g_score[current] + 1

			if neighbour not in openset or tentative_g_score < g_score[neighbour]:
				came_from[neighbour] = current
				g_score[neighbour] = tentative_g_score
				f_score[neighbour] = g_score[neighbour] + heuristic_cost_estimate(neighbour, goal)
				if neighbour not in openset:
					openset.append(neighbour)
	for point in get_direct_neighbours(snake1.head):
		if point in snake1.body or point in snake2.body or 0 >= point.x > board_width-1 or 0 >= point.y > board_height-1:
			continue
		return (get_direction(snake1.head, point), data)
	return 'u'
